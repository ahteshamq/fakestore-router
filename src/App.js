import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import './App.css';
import Products from '../src/components/products'
import Header from './components/header';
import Product from './components/product'
import Cart from './components/Cart';

class App extends Component {
  render() {
    return (
      <Router>
        <div className='App'>
          <Header />
          <Switch>
            <Route path={'/cart'} component={Cart}/>
            <Route exact path='/' component={Products}/>
            <Route exact path='/:id*' component={Product} />
          </Switch>
        </div>
      </Router>

    )
  }
}

export default App;