import axios from 'axios';
import React, { Component } from 'react';
import CartItem from './CartItem';


class Cart extends Component {
    constructor(props) {
        super(props);

        this.state = {
            products: [],
            didReceive: false,
            zeroProduct: null,
            isLoaded: false,
            displayLoader: true,
        };
        this.URL = 'https://fakestoreapi.com/carts';

    }




    fetchData = (url) => {
         axios.get(url)
            .then((response) => {
                if (response.status !== 200) {
                    throw Error("Fetch Failed");
                }
                return response.data
            }).then((data) => {
                if (data.length === 0) {
                    this.setState({
                        zeroProduct: true,
                        displayLoader: false
                    })
                }else{
                    this.setState({
                        products: [...data],
                        isLoaded: true,
                        displayLoader: false
                    })
                }
                
            })
            .catch((error) => {
                this.setState({
                    displayLoader: false,
                    didReceive: true
                })
            })
    }

    componentDidMount = () => {
        this.fetchData(this.URL)
    }

    render() {
        let products = this.state.products;
        return (
            <ul>
                {this.state.displayLoader === true &&
                    <div className="lds-ring"><div></div><div></div><div></div><div></div></div>
                }
                {

                    this.state.isLoaded && products.map((prod) => {
                        return <li className="cart" key={prod.userId.toString()+prod.date}>
                            <CartItem cartProducts = {prod.products} date = {prod.date.slice(0,10)}/>
                        </li>
                    })
                }

                {this.state.didReceive === true &&
                    <div className='error'>
                        <h1>Oops! Cart Could Not Be Loaded</h1>
                    </div>
                }
                {this.state.zeroProduct &&
                    <div className='error'>
                        <h1>Oops! No cart items at this moment</h1>
                    </div>
                }


            </ul>
        );
    }

}

export default Cart;