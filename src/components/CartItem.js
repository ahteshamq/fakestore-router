import React, { Component } from 'react';
import axios from 'axios';
import '../styles/CartItem.css';

class CartItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            products: [],
            didReceive: false,
            zeroProduct: null,
            isLoaded: false,
            displayLoader: true,
        };
        this.URL = 'https://fakestoreapi.com/products';
    }
    fetchData = (url) => {
         axios.get(url)
            .then((response) => {
                if (response.status !== 200) {
                    throw Error("Fetch Failed");
                }
                return response.data
            }).then((data) => {
                if (data === '') {
                    this.setState({
                        zeroProduct: true,
                        displayLoader:false
                    })
                }else{
                    this.setState({
                        products: [...data],
                        isLoaded: true,
                        displayLoader: false
                    })
                }
                
            })
            .catch((error) => {
                this.setState({
                    displayLoader: false,
                    didReceive: true
                })
            })
    }

    componentDidMount = () => {
        this.fetchData(this.URL)
    }

    render() {
        let productList = this.state.products
        let grandTotal = 0
        let cartItems = this.props.cartProducts
        return (<div className='container'>
            {this.state.displayLoader === true &&
                <div className="lds-ring"><div></div><div></div><div></div><div></div></div>
            }
            {this.state.isLoaded && <div className="cartItem">
                <p><b>Date: {this.props.date}</b></p>
                {cartItems.map((item) => {
                    let currentProduct = productList.filter(prod => {
                        return prod.id === item.productId
                    })
                    let total = parseInt(item.quantity) * parseFloat(currentProduct[0].price)
                    grandTotal += total
                    return <div className="itemRow" key={currentProduct[0].title}>
                        <img className='cart-image' src={currentProduct[0].image} alt={currentProduct[0].title}></img>
                        <h4>{currentProduct[0].title}</h4>
                        <div><p>Quantity: {item.quantity} x Price: {currentProduct[0].price}</p></div>
                        <span><b>Total {total}</b></span>
                    </div>
                })
                }
                <div className="grandTotal">
                    <span><b>Grand Total {grandTotal}</b></span>
                </div>
            </div>
            }
            {this.state.didReceive === true &&
                <div className='error'>
                    <h1>Oops! Cart Could Not Be Loaded</h1>
                </div>
            }
            {this.state.zeroProduct &&
                <div className='error'>
                    <h1>Oops! No products at this moment</h1>
                </div>
            }
        </div>);
    }
}

export default CartItem;