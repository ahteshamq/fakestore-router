import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import '../styles/header.css'

class Header extends Component {

    render() {
        return (
            <header>
                <div className="app-logo">
                    <i className="fa-solid fa-store"></i> FAKE STORE
                </div>
                <nav className="menu">
                    <ul>
                        <li><Link to={'/'}>Products</Link></li>
                        <li><Link to={'/cart'}>Cart</Link></li>
                        <li><a href="#">Sign Up</a></li>
                    </ul>
                </nav>
            </header>
        );
    }
}

export default Header;