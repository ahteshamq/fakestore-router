import React, { Component } from 'react';
import axios from 'axios';
import '../styles/products.css'
import { Link } from 'react-router-dom';




class Products extends Component {

    constructor(props) {
        super(props);
        this.state = {
            products: [],
            didReceive: false,
            zeroProduct: null,
            isLoaded: false,
            displayLoader: true,
        };
        this.URL = 'https://fakestoreapi.com/products';
    }

    fetchData = (url) => {
         axios.get(url)
            .then((response) => {
                if (response.status !== 200) {
                    throw Error("Fetch Failed");
                }
                return response.data
            }).then((data) => {
                if (data === '') {
                    this.setState({
                        zeroProduct: true
                    })
                }else{
                    this.setState({
                        products: [...data],
                        isLoaded: true,
                        displayLoader: false
                    })
                }
                
            })
            .catch((error) => {
                this.setState({
                    displayLoader:false,
                    didReceive: true
                })
            })
    }

    componentDidMount = () => {
        this.fetchData(this.URL)
    }

    render() {
        let products = this.state.products;
        return (
            <ul>
                {this.state.displayLoader === true &&
                    <div className="lds-ring"><div></div><div></div><div></div><div></div></div>
                }
                {

                    this.state.isLoaded && products.map((prod) => {
                        return <Link to={`/${prod.id}`} key={prod.id}>
                            <li className="cart">
                                <div className='img'><img src={prod.image} alt={prod.title}></img></div>
                                <div className='product'>
                                    <h2>{prod.title}</h2>
                                </div>
                                <div className="f-rate">
                                    <span><b>Price: {prod.price}</b></span>
                                </div>
                            </li>
                        </Link>
                    })
                }

                {this.state.didReceive === true &&
                    <div className='error'>
                        <h1>Oops! Products Could Not Be Loaded</h1>
                    </div>
                }
                {this.state.zeroProduct &&
                    <div className='error'>
                        <h1>Oops! No products at this moment</h1>
                    </div>
                }


            </ul>
        );
    }
}

export default Products;