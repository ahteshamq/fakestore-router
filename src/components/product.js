import React, { Component } from 'react';
import axios from 'axios';
import '../styles/product.css'




class Product extends Component {

    constructor(props) {
        super(props);
        this.ID = this.props.match.params.id
        this.state = {
            products: null,
            didReceive: false,
            zeroProduct: null,
            isLoaded: false,
            displayLoader: true,
        };

        this.URL = `https://fakestoreapi.com/products/${this.ID}`;
    }

    fetchData = (url) => {
        axios.get(url)
            .then((response) => {
                if (response.status !== 200) {
                    throw Error("Fetch Failed");
                }
                return response.data
            }).then((data) => {
                if (data === '') {
                    this.setState({
                        zeroProduct: true,
                        displayLoader: false
                    })
                } else {
                    this.setState({
                        products: data,
                        isLoaded: true,
                        displayLoader: false
                    })
                }

            })
            .catch((error) => {
                this.setState({
                    didReceive: true,
                    displayLoader: false
                })
            })
    }


    componentDidMount = () => {
        this.fetchData(this.URL)

    }

    render() {
        let prod = this.state.products
        return (
            <div>
                {this.state.displayLoader === true &&
                    <div className="lds-ring"><div></div><div></div><div></div><div></div></div>
                }
                {

                    this.state.isLoaded && <div className="cart">
                        <div className='img'><img src={prod.image} alt=''></img></div>
                        <div className='product'>
                            <h2>{prod.title}</h2>
                            <p className='details'>{prod.description}
                            </p>
                            <div className='description'>
                                <h3>{prod.category}</h3>
                            </div>
                            <div className='action-button'>
                                <span><i className="fa fa-star"></i> {prod.rating.rate}</span>
                                <span> | </span>
                                <span><i className="fa-solid fa-user"></i> {prod.rating.count}</span>
                            </div>
                        </div>
                        <div className="f-rate">
                            <span><b>Price: {prod.price}</b></span>
                        </div>
                    </div>
                }

                {this.state.didReceive &&
                    <div className='error'>
                        <h1>Oops! Products Could Not Be Loaded</h1>
                    </div>
                }
                {this.state.zeroProduct &&
                    <div className='error'>
                        <h1>Oops! Product is not available.</h1>
                    </div>
                }



            </div>
        );
    }
}

export default Product;


// console.log(parseInt(this.props.match.url))
//         let index = parseInt(this.props.match.params.id) - 1
//         console.log(index)
//         let prod = this.state.products[index];